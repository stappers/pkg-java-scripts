#!/bin/bash
#
# Update the Standards-Version field in debian/control
#

STANDARDS_VERSION=4.1.4

echo "Updating Standards-Version to $STANDARDS_VERSION..."
echo ""

grep -q "Standards-Version: $STANDARDS_VERSION" debian/control
if [ $? -eq 0 ]; then
    echo "Standards-Version is up to date"
    exit
fi

# Update debian/control
sed -i -e "s/Standards-Version:.*/Standards-Version: $STANDARDS_VERSION/g" debian/control

# Add an entry to debian/changelog
MSG="Standards-Version updated to $STANDARDS_VERSION"
dch $MSG

# Prepare to commit and review the changes
git add debian/control debian/changelog
git diff --staged

echo ""

while true; do
    read -p "Commit changes? [yN] " choice
    case $choice in
        [Yy]* ) break;;
        * )
          # Revert the staged changes
          git reset HEAD debian/control debian/changelog
          git checkout debian/control debian/changelog
          exit
          ;;
    esac
done

git commit -m "$MSG"
