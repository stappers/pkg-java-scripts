#!/bin/sh
#
# Report a new ITP bug and send a copy to the Debian Java mailing list
#

reportbug --list-cc debian-java@lists.debian.org --offline wnpp
