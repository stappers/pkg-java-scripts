#!/bin/bash
#
# Update the Vcs-* fields in debian/control
#

echo "Updating the Vcs-* fields..."
echo ""


VCS_GIT_UPDATED=0
grep -q 'Vcs-Git: git' debian/control
if [ $? -eq 0 ]; then
    VCS_GIT_UPDATED=1
fi

VCS_BROWSER_UPDATED=0
grep -q 'Vcs-Browser: http://' debian/control
if [ $? -eq 0 ]; then
    VCS_BROWSER_UPDATED=1
fi

TEAMS="pkg-java pkg-clojure debian-med collab-maint"

for TEAM in $TEAMS; do
    sed -i "s#git://anonscm.debian.org/$TEAM/\([^/]*\)/\?#https://anonscm.debian.org/git/$TEAM/\1#g" debian/control
    sed -i "s#git://git.debian.org/$TEAM/\([^/]*\)/\?#https://anonscm.debian.org/git/$TEAM/\1#g" debian/control
    sed -i "s#git://git.debian.org/git/$TEAM/\([^/]*\)/\?#https://anonscm.debian.org/git/$TEAM/\1#g" debian/control
    sed -i "s#http://anonscm.debian.org/cgit/$TEAM/\([^/]*\)/\?#https://anonscm.debian.org/cgit/$TEAM/\1#g" debian/control
    sed -i "s#http://anonscm.debian.org/gitweb/?p=$TEAM/\([^/]*\)/\?#https://anonscm.debian.org/cgit/$TEAM/\1#g" debian/control
    sed -i "s#http://git.debian.org/?p=$TEAM/\([^/]*\)/\?#https://anonscm.debian.org/cgit/$TEAM/\1#g" debian/control
done

# Add an entry to debian/changelog
if [ $VCS_GIT_UPDATED == 1 ] && [ $VCS_BROWSER_UPDATED == 1 ]; then
    MSG="Use secure Vcs-* URLs"
elif [ $VCS_GIT_UPDATED == 1 ]; then
    MSG="Use a secure Vcs-Git URL"
elif [ $VCS_BROWSER_UPDATED == 1 ]; then
    MSG="Use a secure Vcs-Browser URL"
else
    echo "Vcs-* fields are up to date"
    exit;
fi

dch $MSG

# Prepare to commit and review the changes
git add debian/control debian/changelog
git diff --staged

echo ""

while true; do
    read -p "Commit changes? [yN] " choice
    case $choice in
        [Yy]* ) break;;
        * )
          # Revert the staged changes
          git reset HEAD debian/control debian/changelog
          git checkout debian/control debian/changelog
          exit
          ;;
    esac
done

git commit -m "$MSG"
