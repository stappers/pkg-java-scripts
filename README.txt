Helper scripts for maintaining the Java packages
------------------------------------------------

This repository contains various scripts to ease the maintenance work on
the Java packages. It doesn't contain build tools like javahelper or
maven-debian-helper, but only short scripts to update the packaging
repositories.

Here are the scripts available:

 * reportwnpp.sh
        Report a new ITP bug and send a copy to the Debian Java mailing list

 * setup-packaging-repository.sh
        Initialize a packaging repository. The script must be run from the
        package directory and expects an upstream tarball in the parent
        directory. It creates a new Git repository ready to be pushed to Salsa.

 * setup-salsa-repository
        Setup a new Git repository on Salsa

 * dh11.sh
        Update the debhelper compatibility level. The script updates
        debian/control, debian/compat, debian/rules and debian/changelog.
        The changes are reviewed before being committed to Git.

 * stdver.sh
        Update the Standards-Version field in debian/control

 * vcsfields.sh
        Update the Vcs-* field using secure https:// URLs

 * salsavcs.sh
        Update the Vcs-* fields to salsa.debian.org in debian/control

 * gbp-import-orig.sh
        Import an upstream tarball into the current package. The script
        automatically resets the state of the working copy and calls
        gbp import-orig

 * qbts.sh
        Query the Bug Tracking System and display the bugs for
        the current package

 * tag-release.sh
        Tag the current package in Git and push the tags

 * display-manifest
        Display the manifest of a jar


TODO
----

* script removing MIA maintainers
* main update script calling dh11.sh and stdver.sh
