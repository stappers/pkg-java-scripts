#!/bin/bash
#
# Initializes a packaging repository
#

PACKAGE=$(dpkg-parsechangelog | sed -rne 's/^Source: (.*)/\1/p')
VERSION=$(dpkg-parsechangelog | sed -rne 's/^Version: ([0-9.A-Za-z~]+)[-+].*$$/\1/p')
#VERSION=1.0~alpha-1
TEAM=java-team

echo "Package: $PACKAGE"
echo "Version: $VERSION"

if [ -f ../${PACKAGE}_${VERSION}.orig.tar.xz ]; then
    TAR=../${PACKAGE}_${VERSION}.orig.tar.xz
elif [ -f ../${PACKAGE}_${VERSION}.orig.tar.gz ]; then
    TAR=../${PACKAGE}_${VERSION}.orig.tar.gz
elif [ -f ../${PACKAGE}_${VERSION}.orig.tar.bz2 ]; then
    TAR=../${PACKAGE}_${VERSION}.orig.tar.bz2
else
    echo "Upstream tarball not found"
    exit 1;
fi

echo "Tarball: $TAR"

echo ""
echo "Cleanup the working directory..."
shopt -s extglob
rm -Rfv !(debian)

echo ""
echo "Prepating the local repository..."
rm -Rf .git* .travis* .pc .idea .settings .classpath .project .checkstyle .lein-failures .dockerignore
git init
git remote add origin git+ssh://salsa.debian.org/$TEAM/$PACKAGE
git add debian
git commit -m "Initial packaging"

# Create the uptream branch
git checkout --orphan upstream
git rm -rf .
git commit --allow-empty -m 'Initial upstream branch.'
git checkout -f master
gbp import-orig --no-merge --verbose --pristine-tar $TAR
git checkout master
git merge -q --no-edit --allow-unrelated-histories upstream/${VERSION}

while true; do
    read -p "Do you want to push the repository to Salsa now? [yN] " choice
    case $choice in
        [Yy]* ) break;;
        * ) exit;;
    esac
done

. ./setup-salsa-repository ${PACKAGE}

git push --all -u
git push --tags
