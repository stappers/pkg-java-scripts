#!/bin/sh
#
# Import an upstream tarball into the current package
# The script automatically resets the state of the working copy
# and calls gbp import-orig
#
# usage: gbp-import-orig.sh foo_1.1.orig.tar.xz
#

quilt pop -a
rm -Rf .pc
git checkout upstream
git checkout master
gbp import-orig --verbose --merge --pristine-tar $1

git branch -va
