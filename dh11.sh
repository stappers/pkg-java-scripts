#!/bin/bash
#
# Update the debhelper compatibility level
#

DH_LEVEL=11

echo "Upgrading package to DH level $DH_LEVEL..."
echo ""

CURRENT_LEVEL=$(cat debian/compat)

if [ $DH_LEVEL == $CURRENT_LEVEL ]; then
    echo "DH level is up to date"
    exit
fi

# Update debian/compat
echo $DH_LEVEL > debian/compat

# Update debian/control
sed -i -e "s/debhelper[^,]*/debhelper (>= $DH_LEVEL)/g" debian/control

# Remove the --buildsystem option from debian/rules
# (Maven builds are detected automaticaly by DH >= 10)
if [ ! -f "build.xml" ] && [ -f "pom.xml" ]; then
   sed -i "s/ --buildsystem=maven//g" debian/rules
fi

#  --parallel is now implicit
sed -i "s/ --parallel//g" debian/rules

# Add an entry to debian/changelog
MSG="Switch to debhelper level $DH_LEVEL"
dch $MSG

# Prepare to commit and review the changes
git add debian/control debian/changelog debian/compat debian/rules
git diff --staged

echo ""

while true; do
    read -p "Commit changes? [yN] " choice
    case $choice in
        [Yy]* ) break;;
        * )
          # Revert the staged changes
          git reset HEAD debian/control debian/changelog debian/compat debian/rules
          git checkout debian/control debian/changelog debian/compat debian/rules
          exit
          ;;
    esac
done

git commit -m "$MSG"
