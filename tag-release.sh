#!/bin/sh
#
# Tag the current package in Git and push the tags
#

rm -f debian/files

gpg -n --clearsign ~/.profile
gbp buildpackage --git-tag-only --git-sign-tags
git push
git push --tags
