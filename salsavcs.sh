#!/bin/bash
#
# Move the Vcs-* fields to salsa.debian.org in debian/control
#

echo "Updating the Vcs-* fields..."
echo ""

VCS_UPDATED=0
grep -q 'anonscm.debian.org' debian/control
if [ $? -eq 0 ]; then
    VCS_UPDATED=1
fi
grep -q 'git.debian.org' debian/control
if [ $? -eq 0 ]; then
    VCS_UPDATED=1
fi

TEAMS="pkg-java pkg-clojure debian-med collab-maint"

for TEAM in $TEAMS; do
    sed -i "s#git://anonscm.debian.org/$TEAM/\([^/]*\)/\?#https://salsa.debian.org/$TEAM/\1#g" debian/control
    sed -i "s#git://git.debian.org/$TEAM/\([^/]*\)/\?#https://salsa.debian.org/$TEAM/\1#g" debian/control
    sed -i "s#git://git.debian.org/git/$TEAM/\([^/]*\)/\?#https://salsa.debian.org/$TEAM/\1#g" debian/control
    sed -i "s#http\(s\)\?://anonscm.debian.org/git/$TEAM/\([^/]*\)/\?#https://salsa.debian.org/$TEAM/\2#g" debian/control
    sed -i "s#http\(s\)\?://anonscm.debian.org/cgit/$TEAM/\([^/]*\)/\?#https://salsa.debian.org/$TEAM/\2#g" debian/control
    sed -i "s#http\(s\)\?://anonscm.debian.org/gitweb/?p=$TEAM/\([^/]*\)/\?#https://salsa.debian.org/$TEAM/\2#g" debian/control
    sed -i "s#http\(s\)\?://git.debian.org/?p=$TEAM/\([^/]*\)/\?#https://salsa.debian.org/$TEAM/\2#g" debian/control
done

# Remove the .git suffix from the Vcs-Browser URL
sed -i "s#\(Vcs-Browser: .*\).git#\1#" debian/control

# Rename the teams
sed -i "s#/pkg-java/#/java-team/#" debian/control
sed -i "s#/pkg-clojure/#/clojure-team/#" debian/control
sed -i "s#/debian-med/#/med-team/#" debian/control
sed -i "s#/collab-maint/#/debian/#" debian/control

# Add an entry to debian/changelog
if [ $VCS_UPDATED == 1 ]; then
    MSG="Use salsa.debian.org Vcs-* URLs"
else
    echo "Vcs-* fields are up to date"
    exit;
fi

dch $MSG

# Prepare to commit and review the changes
git add debian/control debian/changelog
git diff --staged

echo ""

while true; do
    read -p "Commit changes? [yN] " choice
    case $choice in
        [Yy]* ) break;;
        * )
          # Revert the staged changes
          git reset HEAD debian/control debian/changelog
          git checkout debian/control debian/changelog
          exit
          ;;
    esac
done

git commit -m "$MSG"


# Update the git remote
REMOTE_URL=$(git remote get-url origin)
REMOTE_REGEX='.*.debian.org/git/(.*)/(.*)'
if [[ $REMOTE_URL =~ $REMOTE_REGEX ]]
then
    TEAM=${BASH_REMATCH[1]}
    PACKAGE=${BASH_REMATCH[2]}

    TEAM="${TEAM/pkg-java/java-team}"
    TEAM="${TEAM/pkg-clojure/clojure-team}"
    TEAM="${TEAM/debian-med/med-team}"
    TEAM="${TEAM/collab-maint/debian}"

    SALSA_REMOTE_URL="git@salsa.debian.org:$TEAM/$PACKAGE"

    echo "Changing remote URL to $SALSA_REMOTE_URL"

    git remote remove origin
    git remote add origin $SALSA_REMOTE_URL
    git fetch
    git branch --set-upstream-to=origin/master master
    git branch --set-upstream-to=origin/upstream upstream
    git branch --set-upstream-to=origin/pristine-tar pristine-tar

    echo ""

    git remote -v
fi
